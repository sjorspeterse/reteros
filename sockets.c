// todo:
//  -finish example from unix(7)
//  -finish example from Networking in UNIX
//  -try unix datagramc
//  -try broadcast: SO_BROADCAST in man 7 socket en
//  https://stackoverflow.com/questions/11135801/how-to-broadcast-message-using-udp-sockets-locally
//  -handle ABSTRACT case
//  -handle both UNIX and INET cases

#include <sys/socket.h>     // AF_LOCAL, ..., SOCK_STREAM
#include <unistd.h>         // close, unlink
#include <sys/un.h>         // sockaddr_un
#include <stdio.h>          // printf
#include <stdlib.h>         // calloc, free

#include "sockets.h"
#include "diagnostics.h"

#define NUM_CONNECTIONS 20

enum BindMode {
    UNBOUND,    // only applicable for inet stream clients (?)
    PATH,       // filesystem path
    ABSTRACT    // only applicable for unix domains
};

SocketConfig socket_initialize() {
    return (SocketConfig){
        .domain = AF_INET,
        .type = SOCK_STREAM,
        .bind_mode = UNBOUND,
        .identifier = "",
        .file_descriptor = 0
    };
}

void create_socket(SocketConfig* config) {
    int socket_fd = socket(config->domain, config->type, 0);
    if(socket_fd == -1) printerr(CTX);
    config->file_descriptor = socket_fd;
    printf("socket file descripter %i\n", socket_fd);
}

// Internal function, see unix(7)
// Dynamically allocated, remember to free
struct sockaddr* _bind_socket_unix(
        const SocketConfig* config,
        socklen_t *addrlen) {

    *addrlen = sizeof(struct sockaddr_un);

    struct sockaddr_un* name = calloc(1, *addrlen);
    if(name == NULL) {
        printd("Error allocating memory for unix socket", CTX);
        exit(1);
    }

    name->sun_family = AF_UNIX;
    strncpy(name->sun_path, config->identifier, sizeof(name->sun_path) - 1);
    return (struct sockaddr*) name;
}

void bind_socket(const SocketConfig* config) {
    struct sockaddr* addr;
    socklen_t addrlen;

    switch(config->bind_mode) {
        case UNBOUND: return;
        case PATH: {
            addr = _bind_socket_unix(config, &addrlen);
            break;
        }
        case ABSTRACT: {
            printf("Abstract socket binding not supported yet\n");
            printerr(CTX);
            break;
        }
        default: {
            printf("Unknown socket bind mode: %i\n", config->bind_mode);
        }
    }

    printf("Binding file descriptor %i to %s\n",
            config->file_descriptor, config->identifier);

    if(bind(config->file_descriptor, addr, addrlen))
        printerr(CTX);

    free(addr);
}

void listen_socket(const SocketConfig* config) {
    if(config->type == SOCK_DGRAM) {
        printf("Datagram sockets are connectionless and cannot listen\n");
        return;
    }
    printf("Listening to connections\n");
    if(listen(config->file_descriptor, NUM_CONNECTIONS) == -1) printerr(CTX);
}

int accept_socket(const SocketConfig* config) {
    int data_socket = accept(config->file_descriptor, NULL, NULL);
    if (data_socket == -1) printerr(CTX);
    return data_socket;
}

void connect_socket(const SocketConfig* config, const char* address) {
    struct sockaddr_un addr; //Todo: make this function work for non-unix addresses
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, address, sizeof(addr.sun_path) - 1);

    int ret = connect(config->file_descriptor, (const struct sockaddr *) &addr, sizeof(addr));   
    if(ret == -1)
        printerr(CTX);
}

void close_socket(const SocketConfig* config) {
    printf("Closing file descriptor %i\n", config->file_descriptor);
    if(close(config->file_descriptor) == -1) printerr(CTX);

    if(config->bind_mode == UNBOUND) return;
    printf("Unlinking %s\n", config->identifier);
    if(unlink(config->identifier) == -1) printerr(CTX);
}

void socket_set_domain_unix(SocketConfig* config) {
    config->domain = AF_UNIX;
}

void socket_set_domain_internet(SocketConfig* config) {
    config->domain = AF_INET;
}

void socket_set_type_datagram(SocketConfig* config) {
    config->type = SOCK_DGRAM;
}

void socket_set_type_stream(SocketConfig* config) {
    config->type = SOCK_STREAM;
}

void socket_bind_path(SocketConfig* config, const char* path) {
    config->bind_mode = PATH;
    config->identifier = path;
}

void socket_bind_abstract(SocketConfig* config, const char* name) {
    config->bind_mode = ABSTRACT;
    config->identifier = name;
}
