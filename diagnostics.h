#ifndef DIAGNOSTICS_H
#define DIAGNOSITICS_H

typedef struct {
    const char* file;
    const char* func;
    int line;
} Context;

#define CTX (Context){__FILE__, __func__, __LINE__}

void printd(const char* message, Context ctx);
void printerr(Context ctx);


#endif

