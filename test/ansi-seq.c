#include <stdio.h>
//#include <unistd.h>
#include <string.h>

#define RED 31
#define GREEN 32
#define YELLOW 33
#define BLUE 34
#define PURPLE 35
#define LIGHTBLUE 36

#define RED_BG 41
#define GREEN_BG 42
#define YELLOW_BG 43
#define BLUE_BG 44
#define PURPLE_BG 45
#define LIGHTBLUE_BG 46

#define BOLD 1
#define FAINT 2
#define ITALIC 3
#define UNDERLINE 4
#define BLINK 5
#define INVERT 7
#define INVISIBLE 8
#define STRIKETRHOUGH 9
#define DOUBLEUNDERLINE 21

void setStyle(int format) {
    printf("\033[%dm", format);
}

void defaultStyle() {
    printf("\033[m");
}

void clearScreen() {
    printf("\033[2J");
}

void moveCursor(int line, int col){
    printf("\033[%d;%dH", line, col);
}

void drawPicture(int line, int col, int height, int width, const char *picture) {
    char strline[width+1];
    for(int h=0; h<height; h++){
        moveCursor(line+h, col);
        strncpy(strline, picture+(h*width), width);
        strline[width]='\0';
        printf("%s", strline);
   }
}

void placeChar(int line, int col, char c){
    moveCursor(line, col);
    printf("%c", c);
}

void printCard(int col, char name, char symbol) {
    const char *picture = 
        "-------------"
        "|           |"
        "|           |"
        "|           |"
        "|           |"
        "|           |"
        "|           |"
        "|           |"
        "-------------";
    drawPicture(1, col*14+1, 9, 13, picture);
    const char *line = 
        "           "
        "           "
        " ****    **"
        "**  **   * "
        "     *  ** "
        "     ****  "
        "           ";
    setStyle(RED);
    drawPicture(2, col*14+2, 7, 11, line);
    defaultStyle();
    placeChar(2, col*14+3, name);
    placeChar(3, col*14+3, symbol);
    placeChar(7, col*14+11, symbol);
    placeChar(8, col*14+11, name);
}

void showAllStyles() {
    int i, j, n;

    for (i = 0; i < 11; i++) {
        for (j = 0; j < 10; j++) {
            n = 10 * i + j;
            if (n > 108) break;
            printf("\033[%dm %3d\033[m", n, n);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    clearScreen();
    printCard( 0, 'J','*');
    printCard( 1, 'K','&');
    printCard( 2, 'Q','~');
    moveCursor(11, 0);
    showAllStyles();
    defaultStyle();
    printf("This text is ");
    setStyle(BOLD);
    setStyle(ITALIC);
    setStyle(UNDERLINE);
    printf("bold");
    defaultStyle();
    printf("___");
    printf(" to make it stand out\n");
    defaultStyle();

    return 0;
}
