#ifndef SOCKETS_H
#define SOCKETS_H

typedef struct {
    int domain;    // Description of domain. TODO: delete SocketDomain type and others, we can handle ourselves.
    int type;
    int bind_mode;
    const char* identifier;
    int file_descriptor;
} SocketConfig;

// Initialize socket with a default configuration.
// This is your starting point.
SocketConfig socket_initialize();

// Unix inter-process communication
void socket_set_domain_unix(SocketConfig* config);

// Internet socket
void socket_set_domain_internet(SocketConfig* config);

// A datagram preserves message boundaries
void socket_set_type_datagram(SocketConfig* config);

// A stream has no message boundaries
void socket_set_type_stream(SocketConfig* config);

// Bind socket to a unix filepath
void socket_bind_path(SocketConfig* config, const char* path);

// Bind socket to an abstract namespace
void socket_bind_abstract(SocketConfig* config, const char* name);

// Create socket based on configured domain and type
void create_socket(SocketConfig* config);

// Close socket after usage
void close_socket(const SocketConfig* config);

// Bind socket based on configured bind mode.
// See bind(2)
void bind_socket(const SocketConfig* config);

// Prepare for accepting connections. While one request is being 
// processed other requests can be waiting.
void listen_socket(const SocketConfig* config);

// Returns file descriptor to the data socket
int accept_socket(const SocketConfig* config);

// Connect to a server socket
void connect_socket(const SocketConfig* config, const char* address);

#endif
