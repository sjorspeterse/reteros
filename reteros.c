/* standard libary */
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>

/* POSIX */
#include <unistd.h>

/* nonstandard */
#include <termios.h>

#define BUTTON_R 3  // row of the first button
#define BUTTON_C 44 // column of the buttons

#define TERMINAL_HEIGHT 20

// terminal display width
#define TERDIS_R 3  // start row
#define TERDIS_C 4  // start column
#define TERDIS_W 38 // width
#define TERDIS_H 14 // height

#define GREEN 32

void moveCursor(int line, int col){
    printf("\033[%d;%dH", line, col);
}

void init_scope_string_old() {
   FILE *fp;
   char str[12000];

   fp = fopen("scope.txt" , "r");
   if(fp == NULL) {
      perror("Error opening file");
      return;
   }
   while(fgets(str, 250, fp)) {
      printf("%s", str);
   }
   fclose(fp);
}

char* read_file(const char* filename) {
    char* str;
    FILE *fh = fopen(filename, "rb");
    if(fh != NULL) {
        fseek(fh, 0L, SEEK_END);
        long s = ftell(fh);
        rewind(fh);
        str = malloc(s);
        if(str != NULL) {
          fread(str, s, 1, fh);
          fclose(fh); fh = NULL;
        } else {
            return "";
        }
        if (fh != NULL) {
            fclose(fh);
        }
    }
    return str;
}

const char* scope_old = "\
┌───────────────────────────────────────────────────────────┐\n\
│ ┌───────200us──────────500mV───────────┐     RETEROS      │\n\
│ │      .      .      .      .      .   │ ____     _-'-_   │\n\
│ │      .      .      .      .      .   │ \\__/ a  /     \\  │\n\
│ │......................................│ ____    \\     /  │\n\
│ │      .      .      .     **      .   │ \\__/ b   ¯-_-¯   │\n\
│ │►     .      .      .  *** .**    .   │ ____    VOLTAGE  │\n\
│ │      .      .      . *    .  *****   │ \\__/ c   _-'-_   │\n\
│ │......................*............*..│ ____    /     \\  │\n\
│ │      .      .      .*     .      . * │ \\__/ d  \\     /  │\n\
│ │      .      .      *      .      .  *│ ____     ¯-_-¯   │\n\
│ │      .****  .     *.      .      .   │ \\__/ e   TIME    │\n\
│ │....***....*......*...................│ ____             │\n\
│ │  **  .     *.   *  .      .      .   │ \\__/ f     ▲     │\n\
│ │**    .      ****   .      .      .   │ ____     ◄ x ►   │\n\
│ │      .      .      .      .      .   │ \\__/ g     ▼     │\n\
│ └──────────────────────────────────────┘                  │\n\
└──────┐  ┌───────────────────────────────────────┐  ┌──────┘\n\
       └──┘                                       └──┘\n\
";


static struct termios orig_term;
static struct termios new_term;

void clearScreen() {
    printf("\033[2J\n");
}

void hideCursor() {
    printf("\x1b[?25l");
}

void showCursor() {
    printf("\x1b[?25h");

}

void setupConsole(void) {
    tcgetattr(STDIN_FILENO, &orig_term);
    new_term = orig_term;

    new_term.c_lflag &= ~(ICANON | ECHO);

    tcsetattr(STDIN_FILENO, TCSANOW, &new_term);
    hideCursor();
    clearScreen();

    // set stdin non-blocking
    // fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);
}

void restoreConsole(void) {
    // Reset colors
    printf("\x1b[0m");

    showCursor();

    // Reset console mode
    tcsetattr(STDIN_FILENO, TCSANOW, &orig_term);
}

static inline void msleep(unsigned ms) {
    nanosleep((const struct timespec[]){{0, ms*1000000L}}, NULL);
}

void getCursorPosition(int *row, int *col) {
    printf("\x1b[6n");
    char buff[128];
    int indx = 0;
    for(;;) {
        int cc = getchar();
        buff[indx] = (char)cc;
        indx++;
        if(cc == 'R') {
            buff[indx + 1] = '\0';
            break;
        }
    }
    int temprow, tempcol;
    sscanf(buff, "\x1b[%d;%dR", &temprow, &tempcol);
    if (row != NULL) *row = temprow;
    if (col != NULL) *col = tempcol;

    fseek(stdin, 0, SEEK_END);
}

void scrollUp(int rows) {
    printf("\033[%dS", rows);
}

void scrollDown(int rows) {
    printf("\033[%dT", rows);
}

void resetCursor() {
    moveCursor(1, 1);
}

void clearRow(int row) {
    moveCursor(row, 1);
    printf("\033[2K");
    resetCursor();
}

// clear from the cursor to the end of the screen (and delete history)
void clear() {
    resetCursor();
    printf("\033[3J");
}

void setStyle(int format) {
    printf("\033[%dm", format);
}

void defaultStyle() {
    printf("\033[m");
}

void handle_signal(int sig) {
    // Handler for SIGINT, caused by Ctrl-C at keyboard
    if(sig == SIGINT) {
        restoreConsole();
        moveCursor(TERMINAL_HEIGHT, 1);
        fflush(stdout);
        exit(0);
    }
}

int kbhit() {
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv) > 0;
}

int getch() {
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}

int screenSizeChanged(int *rows, int* cols) {
    int oldRows = *rows;
    int oldCols = *cols;
    moveCursor(2000, 2000);
    getCursorPosition(rows, cols);
    resetCursor();
    return (oldRows != *rows || oldCols != *cols);
}

void limit(int low, int *value, int high) {
    if(*value < low) *value = low;
    if(*value > high) *value = high;
}

int max(int v1, int v2) {
    if(v1 > v2) return v1;
    else return v2;
}

int min(int v1, int v2) {
    if(v1 < v2) return v1;
    else return v2;
}

void redraw(int rows, int cols) {
    int error = 0;

    //start_row = min(start_row, rows-scope_height+1);
    //start_row = max(start_row, 1);

    /*
    moveCursor(start_row, 1);
    printf("----");
    printf("Start row: %i, rows: %i, scope height: %i", start_row, rows, scope_height);
    if(error) clear();
    
    for(int i=1; i<scope_height-1; i++) {
        moveCursor(start_row+i, 1);
        printf("    ");
    }
    moveCursor(start_row+scope_height-1, 1);
    printf("----");
    */ 
    resetCursor();
}

void fillScope(char c) {
    for(int row=3; row<17; row++) {
        moveCursor(row,4);
        for(int i=0; i<TERDIS_W; i++) {
            putchar(c);
        }
        putchar('\n');
    }
}
void clearScope() {
    fillScope(' ');
}

void getCharPixels(int *height, int *width) {
    printf("\033[16t");
    fflush(stdout);
    char buff[128];
    int indx = 0;
    for(;;) {
        int cc = getchar();
        buff[indx] = (char)cc;
        indx++;
        if(cc == 't') {
            buff[indx + 1] = '\0';
            break;
        }
    }
    int tempheight, tempwidth;
    sscanf(buff, "\x1b[6;%d;%dt", &tempheight, &tempwidth);
    if (height != NULL) *height = tempheight;
    if (width != NULL) *width = tempwidth;

    fseek(stdin, 0, SEEK_END);
}

void fillScopeSixel(int, int);

int main() {
    signal(SIGINT, handle_signal);
    setupConsole();
    resetCursor();
    char *scope = read_file("scope.txt");
    char states[7] = {0};
    int i = 0;
    printf("%s", scope);
    clearScope();
    while(1) {
        while(!kbhit()) {
            double t = i++/9.0;
            double y = 10 + 6*sin(t);
            moveCursor(y, (4+(i%TERDIS_W)));
            putchar('*');
            //if(i%TERDIS_W==0) clearScope();
            fflush(stdout);
            msleep(20);
        }
        char mychar = getch();
        if(mychar >= 'a' && mychar <= 'g') {
            int index = mychar - 'a';
            states[index] = (states[index]+1)%2;
            if(states[index]) setStyle(GREEN);
            moveCursor(BUTTON_R+2*index, BUTTON_C);
            printf("____");
            moveCursor(BUTTON_R+1+2*index, BUTTON_C);
            printf("\\__/");
        }
        if(mychar == 'u') scrollUp(1);
        if(mychar == 'U') scrollDown(1);
        if(mychar == 'w') clear();
        if(mychar == 't') {
            int charh, charw;
            getCharPixels(&charh, &charw);
            fillScopeSixel(charh, charw);
        }
        if(mychar == 'q') break;
        defaultStyle();


        fflush(stdout);
    }
    
    restoreConsole();
    moveCursor(TERMINAL_HEIGHT,1);
    free(scope);
    return 0;
}

void fillScopeSixel(int charh, int charw) {
    moveCursor(TERDIS_R, TERDIS_C);
    int nrFullSixelRows = (charh*TERDIS_H)/6;
    int nrLastLines = (charh*TERDIS_H)%6;
    int nrSixelRows = nrFullSixelRows + (nrLastLines > 0);
    int nrSixelCols = charw*TERDIS_W+1;
    char allSix = '~';
    char firstFew = '?' + (0x3f>>(6-nrLastLines));
    char newline = '-';
    int buffSize = nrSixelRows*nrSixelCols;
    char sixelBuf[buffSize];

    /*
    printf("\ePq");
    for(int i=0; i < nrFullSixelRows; i++) {
        printf("!%i%c-", nrSixelCols, allSix);
    }
    printf("!%i%c", nrSixelCols, firstFew);
    printf("\e\\");
    */

    for(int i=0; i<buffSize-1; i++)
        sixelBuf[i] = allSix;

    for(int i=0; i<nrSixelRows-1; i++)
        sixelBuf[nrSixelCols*i + nrSixelCols-1] = newline;
    if(nrLastLines > 0)
        for(int i=0; i<nrSixelCols-1; i++)
            sixelBuf[nrFullSixelRows*nrSixelCols+i] = firstFew;
    sixelBuf[buffSize-1] = '\0';

    printf("\ePq%s\e\\", sixelBuf);
    printf("%i %i %i", nrLastLines, charw, nrSixelCols);
}
