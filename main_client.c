/*
 * Showcase how sockets may be used
 * Useful commands:
 * - lsof (-U)        This lists open files (unix sockets)
 *
 * Some properties:
 * UDS (Unix Domain Socket):
 */
#include <stdio.h>          // printf
// #include <stdlib.h>
// #include <string.h>
// #include <arpa/inet.h>
// #include <sys/types.h>
// #include <netinet/in.h>

#include "sockets.h"        // SocketConfig

#define NUM_CONFIGS 2

int run_client() {
    SocketConfig configs[NUM_CONFIGS];

    for(int i=0; i<NUM_CONFIGS; i++) {
        configs[i] = socket_initialize();
    }
    SocketConfig *unix_datagram = configs;
    socket_set_domain_unix(unix_datagram);
    socket_set_type_datagram(unix_datagram);

    SocketConfig *unix_stream = configs+1;
    socket_set_domain_unix(unix_stream);
    socket_set_type_stream(unix_stream);
    
    // configs[0] = (SocketConfig) {UNIX_DOMAIN, DATAGRAM_TYPE, UNBOUND};
    // configs[1] = (SocketConfig) {UNIX_DOMAIN, STREAM_TYPE, UNBOUND};
    // configs[2] = (SocketConfig) {INTERNET_DOMAIN, DATAGRAM_TYPE, UNBOUND};
    // configs[3] = (SocketConfig) {INTERNET_DOMAIN, STREAM_TYPE, UNBOUND};

    for(int i=0; i<NUM_CONFIGS; i++) {
        create_socket(configs+i);
    }

    connect_socket(unix_stream, "/tmp/sjors_unix_stream");
    
    // read and print
    //char buffer[12];
    double buffer[1];
    #include<unistd.h>
    while(1) {
        int ret = read(unix_stream->file_descriptor, buffer, sizeof(buffer));
        if(ret == -1) {
            perror("read");
            break;
        }
        if(ret>0) {
            //printf("Response from server: %s.\n", buffer);
            printf("Response from server: %.3f.\n", *buffer);
        }
    }


    for(int i=0; i<NUM_CONFIGS; i++) {
        close_socket(configs+i);
    }
    
    return configs[0].file_descriptor;
}

int main() {
    int client = run_client();
}
