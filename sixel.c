#include <stdio.h>
#include <unistd.h>

void getCharPixels(int *height, int *width) {
    printf("\033[16t");
    fflush(stdout);
    char buff[128];
    int indx = 0;
    for(;;) {
        int cc = getchar();
        buff[indx] = (char)cc;
        indx++;
        if(cc == 't') {
            buff[indx + 1] = '\0';
            break;
        }
    }
    int tempheight, tempwidth;
    sscanf(buff, "\x1b[6;%d;%dt", &tempheight, &tempwidth);
    if (height != NULL) *height = tempheight;
    if (width != NULL) *width = tempwidth;

    fseek(stdin, 0, SEEK_END);
}

int main() {
    // single pixel (implicit aspect ratio of 3)
    printf("\ePq@\e\\");

    // image with explicit aspect ratio 1:1, image is (hor/ver, 1/1)
    printf("\ePq\"1;1;1;1@-\e\\");


    // box
    printf("\ePq~!200@~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200?~-\
                ~!200_~-\
            \e\\");

    // All possiblities
    printf("\ePq@ABCDEFGHIJKLMNOPQRSTUVWXYZ[^_`abcdefghijklmnopqrstuvwxyz{|}~\e\\");
    
    // Double in two statements
    printf("\ePq#1@ABCDEFGHIJKLMNOPQRSTUVWXYZ[^_`abcdefghijklmnopqrstuvwxyz{|}~-");
    printf("@ABCDEFGHIJKLMNOPQRSTUVWXYZ[^_`abcdefghijklmnopqrstuvwxyz{|}~\e\\");
    
    // image with (implicit) aspect ratio 3 pixels
    printf("\ePq#0;2;60;0;0#0@-\e\\");

    // image with explicit aspect ratio 1:1, image is (hor/ver, 93/14)
    printf("\ePq\"1;1;93;14#0;2;60;0;0#0@-\e\\");

   
    // Newline: '-'
    // Carriage return: '$'
    // Emtpy sixel: '?', then '@' to '~'

    // "HI" (color define)
    printf("\ePq#0;2;0;0;0#1;2;100;100;0#2;2;0;100;0#1~~@@vv@@~~@@~~$#2??}}GG}}??}}??-#1!14@\e\\");

    int height, width;
    getCharPixels(&height, &width);
    printf("Height: %i, width: %i\n", height, width);


    /*
     *
     * m = self._interrogate("\033[16t", "t", "^\033\\[6;([0-9]+);([0-9]+)t$"); print(m)
     *
		sys.stdout.write("\033[16t")
		sys.stdout.flush()

		r = ""
		c = ""
		noinput = 0
		capture = False

		while True:
			c = sys.stdin.read(1)
			if c == "":
				time.sleep(.1)
				noinput += 1
				if noinput > timeout: return
				continue
			noinput = 0
			if not capture:
				if c != "\033": continue
				capture = True

			r = r + c
			if r.endswith("t"): break
			if len(r) >= 65535: return

		if isinstance(answerpattern, str):
			return re.search(answerpattern, r)
		else:
			return answerpattern.search(r)

    */

}
