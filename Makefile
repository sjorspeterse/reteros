c_files := $(filter-out main_server.c main_client.c gui.c, $(wildcard *.c))
h_files := $(wildcard *.h)

all: server client

reteros: reteros.c
	gcc -o go reteros.c -lm

server: main_server.c $(c_files) $(h_files) bindir
	gcc -o bin/server main_server.c $(c_files) -lm

client: main_client.c $(c_files) $(h_files) bindir
	gcc -o bin/client main_client.c $(c_files)

bindir:
	mkdir -p bin

clean:
	rm -rf bin

