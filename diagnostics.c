#include <stdio.h>      // printf
#include <errno.h>      // errno
#include <string.h>     // strerror
#include <stdlib.h>     // exit

#include "diagnostics.h"

void printd(const char* message, Context ctx) {
    printf("%s (%s, function %s, line %i)\n", message, ctx.file, ctx.func, ctx.line);
}

void printerr(Context ctx) {
    fprintf(stderr, "Error in %s, function %s, line %i: %s\n",
            ctx.file, ctx.func, ctx.line, strerror(errno));
    exit(errno);
}
