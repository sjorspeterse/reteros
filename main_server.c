// TODO: Clean up, and allow multiple simultanous connections with non-blocking accepts.
/*
 * Showcase how sockets may be used
 * Useful commands:
 * - lsof (-U)          Lists open files (unix sockets)
 * - netstat (--unix)   Lists (unix) sockets and their state
 *
 * Some properties:
 * UDS (Unix Domain Socket):
 */
#include <stdio.h>          // printf
#include <signal.h>         // signal
#include <stdlib.h>         // exit
#include <math.h>           // sin

// #include <stdlib.h>      // Networking in UNIX example
// #include <string.h>      // Networking in UNIX example
// #include <arpa/inet.h>   // Networking in UNIX example
// #include <sys/types.h>   // Networking in UNIX example
// #include <netinet/in.h>  // Networking in UNIX example

#include "sockets.h"        // SocketConfig

#define NUM_CONFIGS 2
SocketConfig configs[NUM_CONFIGS];

// Close all sockets
void close_all_sockets() {
    printf("Closing all sockets...\n");
    for(int i=0; i<NUM_CONFIGS; i++) {
        if(configs[i].file_descriptor)
            close_socket(configs+i);
    }
}

void handle_signal(int sig) {
    // Handler for SIGINT, caused by Ctrl-C at keyboard
    if(sig == SIGINT) {
        printf("\nCaught SIGINT!\n");
        close_all_sockets();
        exit(0);

    // Handler for SIGINT, caused by client that close their end too quickly
    } else if(sig == SIGPIPE) {
        printf("\nCaught SIGPIPE!\n");
    }
}

void run_server() {
    for(int i=0; i<NUM_CONFIGS; i++) {
        configs[i] = socket_initialize();
    }
    SocketConfig *unix_datagram = configs;
    socket_set_domain_unix(unix_datagram);
    socket_set_type_datagram(unix_datagram);
    socket_bind_path(unix_datagram , "/tmp/sjors_unix_dgram");
    //socket_bind_abstract(unix_datagram , "hoisjors");

    SocketConfig *unix_stream = configs+1;
    socket_set_domain_unix(unix_stream);
    socket_set_type_stream(unix_stream);
    socket_bind_path(unix_stream, "/tmp/sjors_unix_stream");

    for(int i=0; i<NUM_CONFIGS; i++) {
        create_socket(configs+i);
        bind_socket(configs+i);
        listen_socket(configs+i);
    }

    #include <unistd.h>
    #include <sys/time.h>
    struct timeval tv;
    struct timezone tz;
    double t=0.0;
    while(1) {
        printf("Waiting for incoming connection...\n");
        // accept connection
        int data_socket = accept_socket(unix_stream);
        printf("Accepted connection\n");

        // read (receive?)

        double buffer[1];
        int ret = 0;
        while(ret != -1) {
            gettimeofday(&tv, NULL);
            t += 0.01;
            //*buffer = tv.tv_sec + (double)tv.tv_usec/1e6;
            *buffer = sin(t);

            ret = write(data_socket, buffer, sizeof(buffer));
            usleep(10000);
        }
        printf("Error in write\n");
        
        // close connection
        close(data_socket);
        printf("Closed connection\n");
    }
}

int main() {
    signal(SIGINT, handle_signal);
    signal(SIGPIPE, handle_signal);
    run_server();
}
